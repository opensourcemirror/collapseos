# Assembling Collapse OS from within it

This is where we tie lose ends, complete the circle, loop the
loop: we assemble a new Collapse OS *entirely* from within
Collapse OS.

Build Collapse OS' from within Collapse OS is very similar to
how we do it from the build.fs units in ports. Go ahead, open
one and look at it. There is some Dusk OS code as a prelude,
which you can ignore, the real part starts at the line defining
system constants (PS_ADDR, RS_ADDR, etc.)

To assemble Collapse OS from within it, all you need to do is
execute the content of this unit. When you run makefiles, it's
already Collapse OS building itself from within it, so it's not
different when it's the real deal.

When you do so, it will yield a binary in memory. To know the
start/end offset of the binary, You'll use xorg and here. xorg
is where your first byte starts in your host's memory,
"here xorg -" is the size of your binary.

With that, you can write that binary between those offsets on
your target media. That binary should be the exact same as what
you get when you run "make". You now have a new Collapse OS
deployment.

See more details on bootstrapping at doc/bootstrap.
